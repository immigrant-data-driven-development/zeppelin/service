#!/bin/sh
python manage.py collectstatic --noinput
/usr/local/bin/gunicorn gap_analysis_manager.wsgi:application -w 2 -b :8000