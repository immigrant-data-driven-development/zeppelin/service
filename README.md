
# Service

## 🚀 Goal

Process the Questionnaire in excel or Google Form and save the anwser in a database. 

## ⚙️ Requirements

1. Docker Compose
2. Pyhton 3.10

## 🔧 Install

Create a .env with:

DB_ENGINE = django.db.backends.postgresql
DB_NAME = zeppelin
DB_USERNAME = zeppelin
DB_PASSWORD = zeppelin
DB_HOST = localhost
DB_PORT = 5432
DEBUG = True 
SECRET_KEY=ARANDOMSECRETKE
COMPOSE_PROJECT_NAME=zeppelin

The following commnads will create the Database and Django containers:

```bash
docker-compose down --volume
docker-compose build

```
## 💻 Usage

After installing, execute the following command:

```
docker-compose up -d 
```

Click in [http://localhost:8000](http://localhost:8000). 

User and Password = ze ppelin

## 🛠️ Stack

1. Pyhton 3.10
2. Django
3. Postgresql

## ✒️ Team
[Paulo Sérgio dos Santos Júnior](paulossjunior@gmail.com)

## 📕 Literature
1. Santos Júnior, P.S., Barcellos, M.P., Ruy, F.  Tell me: Am I going to Heaven? A Diagnosis Instrument of Continuous Software Engineering Practices Adoption, In: 25th International Conference on Evaluation and Assessment in Software Engineering (EASE), 2021, p. 30–39.

2. Santos Júnior, P.S., Barcellos, M.P., Ruy, F., Omêna, M.  Flying over Brazilian Organizations with Zeppelin: A Preliminary Panoramic Picture of Continuous Software Engineering, In: 36th Brazilian Symposium on Software Engineering (SBES), 2022







