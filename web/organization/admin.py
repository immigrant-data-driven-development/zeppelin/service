from unicodedata import category
from django.contrib import admin
from .models import *


class OrganizationTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'description','category')
    list_display_links = ('name',)
    ordering = ['name']
    search_fields = ['name',]

class OrganizationSizeAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_display_links = ('name',)
    ordering = ['name']
    search_fields = ['name',]


class OrganizationAdmin(admin.ModelAdmin):
    list_display = ('name', 'description',)
    list_display_links = ('name',)
    ordering = ['name']
    search_fields = ['name',]

class StateAdmin(admin.ModelAdmin):
    list_display = ('name', 'region','latitude','longitude',)
    list_display_links = ('name',)
    ordering = ['name']
    search_fields = ['name',]

admin.site.register(Organization,OrganizationAdmin)
admin.site.register(OrganizationType,OrganizationTypeAdmin)
admin.site.register(Size,OrganizationSizeAdmin)
admin.site.register(State,StateAdmin)
admin.site.register(Region)
admin.site.register(OrganizationCategory)
