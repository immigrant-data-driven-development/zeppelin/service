from django.urls import path
from .views import QuestionnaireListView

urlpatterns = [
    path("questionnaire/", QuestionnaireListView.as_view()),   
]