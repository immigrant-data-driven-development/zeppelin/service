from django.contrib import admin
from .models import *

class AdoptedLevelAdmin(admin.ModelAdmin):
    list_display = ('name', 'description','percentage')
    list_display_links = ('name',)
    ordering = ['name', 'percentage']
    search_fields = ['name',]
class StatementLevelAdmin(admin.ModelAdmin):
    list_display = ('code', 'statement','sth_stage', 'pe_element', 'continuous_activity', )
    list_display_links = ('code',)
    ordering = ['code', 'sth_stage', 'pe_element',]
    list_filter = ('sth_stage','pe_element','fcse_processes', )
    search_fields = ['code',]

class QuestionnarieLevelAdmin(admin.ModelAdmin):
    list_display = ('document','uploaded_at', )
    #list_display_links = ('employee',)
    ordering = ['uploaded_at',]
    #list_filter = ('employee', )
    #search_fields = ['employee',]

class QuestionarExcelAdmin(admin.ModelAdmin):
    list_display = ('document','uploaded_at', 'employee')
    list_display_links = ('employee',)
    ordering = ['uploaded_at',]
    list_filter = ('employee', )
    search_fields = ['employee',]

class AnswerAdmin(admin.ModelAdmin):
    list_display = ('organization', 'adopted_level','statement',  )
    list_display_links = ('organization',)
    ordering = ['organization', 'statement', 'adopted_level',]
    list_filter = ('adopted_level', 'organization',)
    search_fields = ['adopted_level',]

admin.site.register(Answer,AnswerAdmin)
admin.site.register(Questionnaire,QuestionarExcelAdmin)
admin.site.register(QuestionnaireExcel,QuestionarExcelAdmin)
admin.site.register(QuestionnaireGoogleForms,QuestionarExcelAdmin)
admin.site.register(Statement,StatementLevelAdmin)
admin.site.register(AdoptedLevel,AdoptedLevelAdmin)
admin.site.register(FeedbackQuestionnaire)
# Register your models here.
