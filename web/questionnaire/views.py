from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic.list import ListView
from .models import Questionnaire

class QuestionnaireListView(ListView):
    model = Questionnaire
    template_name  = 'questionnaire/questionnaire_list.html'
    paginate_by = 10
    ordering = ['employee__organization__name']