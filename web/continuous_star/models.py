from django.db import models

# Create your models here.
from core.models import Base


class ContinuousPhase(Base):
    name = models.CharField(max_length=200)
    description = models.TextField()

    class meta:
        db_table = 'continuous_star_continuous_phase'
        ordering = ['name']
    
    def __str__(self):
        """ String para representar o process"""
        return self.name

class ContinuousActivity(Base):

    name = models.CharField(max_length=200)
    description = models.TextField()

    continuous_phase = models.ForeignKey(ContinuousPhase, on_delete=models.CASCADE)

    def __str__(self):
        """ String para representar o process"""
        return self.name
    class meta:
        db_table = 'continuous_star_continuous_activity'
        ordering = ['name']