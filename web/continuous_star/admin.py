from django.contrib import admin
from .models import *
# Register your models here.


class ContinuousPhaseAdmin(admin.ModelAdmin):
    list_display = ('name', 'description',)
    list_display_links = ('name',)
    ordering = ['name']
    search_fields = ['name']

class ContinuousActivityAdmin(admin.ModelAdmin):
    list_display = ('name', 'description','continuous_phase',)
    list_display_links = ('name',)
    ordering = ['name']
    search_fields = ['name']


admin.site.register(ContinuousPhase,ContinuousPhaseAdmin)
admin.site.register(ContinuousActivity,ContinuousActivityAdmin)