from django.apps import AppConfig


class ContinuousStarConfig(AppConfig):
    name = 'continuous_star'
