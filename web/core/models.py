from django.db import models

# Create your models here.
class Base(models.Model):

    """
    Stores a single blog entry, related to :model:`blog.Blog` and
    :model:`auth.User`.
    """

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    class Meta:
        abstract = True