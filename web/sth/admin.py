from django.contrib import admin
from .models import Stage

class StageAdmin(admin.ModelAdmin):
    list_display = ('name', 'description',)
    list_display_links = ('name',)
    ordering = ['name']
    search_fields = ['name',]

admin.site.register(Stage,StageAdmin)
# Register your models here.
