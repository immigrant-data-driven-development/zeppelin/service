from django.db import models
from core.models import Base

class Stage (Base):

    """
    Represents a stage of StH.
    """

    name = models.CharField(max_length=200,help_text= "stage's name")
    description = models.TextField(help_text= "stage's description")

    class meta:
        db_table = 'sth_stage'
        ordering = ['name']

    def __str__(self):
        """ String para representar o stage"""
        return self.name
