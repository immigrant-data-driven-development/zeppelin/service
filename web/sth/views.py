from django.shortcuts import render

from .models import *
from .serializers import *
from rest_framework import viewsets

class StageViewSet(viewsets.ModelViewSet):
    
    queryset = Stage.objects.all()
    serializer_class = StageSerializer

