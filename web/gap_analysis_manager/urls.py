"""gap_analysis_manager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from cse_framework.views import ProcessViewSet
from practitioners_eye.views import CategoryViewSet
from practitioners_eye.views import ElementViewSet
from sth.views import StageViewSet

from django.conf.urls.static import static
from django.views.static import serve
from django.conf import settings
from django.shortcuts import redirect

router = routers.DefaultRouter()
router.register(r"frameworks/fcse/process", ProcessViewSet)
router.register(r"frameworks/pe/element", ElementViewSet)
router.register(r"frameworks/pe/category", CategoryViewSet)
router.register(r"frameworks/sth/stage", StageViewSet)


urlpatterns = [
    path('admin/doc/', include('django.contrib.admindocs.urls'), name="admin_doc"),
    path('', lambda request: redirect('admin/', permanent=True)),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path("api/", include(router.urls)),
    path("",include('questionnaire.urls')),
    path('media/(<path>.)', serve, {'document_root': settings.MEDIA_ROOT}),
    
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
