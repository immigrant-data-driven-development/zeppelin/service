from django.apps import AppConfig


class CseFrameworkConfig(AppConfig):
    name = 'cse_framework'
