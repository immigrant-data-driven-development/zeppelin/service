from django.contrib import admin
from .models import Process


class ProcessAdmin(admin.ModelAdmin):
    list_display = ('name', 'description',)
    list_display_links = ('name',)
    ordering = ['name']
    search_fields = ['name']
    
admin.site.register(Process,ProcessAdmin)
# Register your models here.
