from django.db import models
from core.models import Base

class Process (Base):

    """
    Represents a process of Continuous Software Engineering Framework.
    """

    name = models.CharField(max_length=200,help_text= "process' name")
    description = models.TextField(help_text= "process' description")

    class meta:
        db_table = 'fcse_process'
        ordering = ['name']

    def __str__(self):
        """ String para representar o process"""
        return self.name

