from django.shortcuts import render

from .models import Process
from .serializers import ProcessSerializer
from rest_framework import viewsets

class ProcessViewSet(viewsets.ModelViewSet):
    
    queryset = Process.objects.all()
    serializer_class = ProcessSerializer
    