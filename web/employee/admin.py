from django.contrib import admin
from .models import *

class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('name', 'organization','email',)
    list_display_links = ('name',)
    list_filter = ('organization',)
    ordering = ['name']
    search_fields = ['name','organization',]

class AcademicDegreeAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_display_links = ('name',)
    list_filter = ('name',)
    ordering = ['name']
    search_fields = ['name',]

class AcademicDegreeStatusAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_display_links = ('name',)
    list_filter = ('name',)
    ordering = ['name']
    search_fields = ['name',]

class KnwoledgeLevelAdmin(admin.ModelAdmin):
    list_display = ('name','description',)
    list_display_links = ('name',)
    list_filter = ('name',)
    ordering = ['name']
    search_fields = ['name',]

class ExperienceLevelAdmin(admin.ModelAdmin):
    list_display = ('name','description',)
    list_display_links = ('name',)
    list_filter = ('name',)
    ordering = ['name']
    search_fields = ['name',]


class EmployeeKnowledgeAdmin(admin.ModelAdmin):
    list_display = ('academic_degree','academic_degree_status','employee',)
    list_display_links = ('academic_degree',)
    list_filter = ('academic_degree',)
    ordering = ['academic_degree']
    search_fields = ['academic_degree',]


admin.site.register(Employee,EmployeeAdmin)

admin.site.register(SthStageKnwoledgeLevel)

admin.site.register(SthStageExperienceLevel)

admin.site.register(EmployeeKnowledge,EmployeeKnowledgeAdmin)

admin.site.register(KnwoledgeLevel,KnwoledgeLevelAdmin)

admin.site.register(ExperienceLevel,ExperienceLevelAdmin)

admin.site.register(AcademicDegree,AcademicDegreeAdmin)

admin.site.register(AcademicDegreeStatus,AcademicDegreeStatusAdmin)

admin.site.register(Position)

admin.site.register(AcademicDegreeCategory)

