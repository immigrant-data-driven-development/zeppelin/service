from django.db import models
from core.models import Base
from organization.models import Organization
from sth.models import Stage


class AcademicDegreeCategory(Base):
    name = models.CharField(max_length=200)

    class meta:
        db_table = 'academic_degree_category'

    def __str__(self):
        
        return self.name

class AcademicDegree(Base):
    """
    Represents an academic degree of Employee
    """
    name = models.CharField(max_length=200)
    category = models.ForeignKey(AcademicDegreeCategory, on_delete=models.CASCADE,null=True, blank=True)

    class meta:
        db_table = 'academic_degree'

    def __str__(self):
        
        return self.name

class AcademicDegreeStatus(Base):
    """
    Represents an academic degree of Employee
    """
    name = models.CharField(max_length=200,help_text= "employee's name")

    class meta:
        db_table = 'academic_degree_status'

    def __str__(self):
        
        return self.name

class KnwoledgeLevel(Base):

    name = models.CharField(max_length=200,help_text= "employee's name")
    description = models.TextField(help_text= "organization type's description")
    value = models.FloatField()
    class meta:
        db_table = 'knowledge_level'

    def __str__(self):
        
        return self.name

class ExperienceLevel(Base):
    
    name = models.CharField(max_length=200,help_text= "employee's name")
    description = models.TextField(help_text= "organization type's description")
    value = models.FloatField()
    
    class meta:
        db_table = 'experience_level'

    def __str__(self):
        
        return self.name

class Position(Base):
    name = models.CharField(max_length=200,help_text= "position")
    
    class meta:
        db_table = 'position'

    def __str__(self):        
        return self.name

class Employee(Base):

    """
    Represents a employee of organization related to :model:`questionnaire.organization`.
    """

    name = models.CharField(max_length=200,help_text= "employee's name")
    email = models.EmailField(unique=True)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE,help_text= "employee's organization",null=True, blank=True)
    position = models.ForeignKey(Position, on_delete=models.CASCADE,help_text= "employee's position",null=True, blank=True)
    role = models.CharField(max_length=200,help_text= "role")
    
    class meta:
        db_table = 'employee'

    def __str__(self):        
        return self.name + " - "+self.email

class EmployeeKnowledge(Base):
    
    academic_degree = models.ForeignKey(AcademicDegree, on_delete=models.CASCADE,help_text= "employee's organization")
    academic_degree_status = models.ForeignKey(AcademicDegreeStatus, on_delete=models.CASCADE,help_text= "employee's organization")
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE,help_text= "employee's organization",null=True, blank=True)
    
    class meta:
        db_table = 'employee_knowledge'

    def __str__(self):
        return self.academic_degree.name +"-"+ self.academic_degree_status.name
    

class SthStageKnwoledgeLevel (Base):
    
    stage = models.ForeignKey(Stage, on_delete=models.CASCADE,help_text= "employee's knowledge")
    knwoledge_level = models.ForeignKey(KnwoledgeLevel, on_delete=models.CASCADE,help_text= "employee's knowledge")
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE,help_text= "employee's knowledge", related_name= "employee_knowledge_level")
    
    class meta:
        db_table = 'sth_stage_knwoledge_level'
    
    def __str__(self):
        return str(self.employee) +"-"+str(self.stage) +"-"+ str(self.knwoledge_level)

    
class SthStageExperienceLevel (Base):
    
    stage = models.ForeignKey(Stage, on_delete=models.CASCADE,help_text= "employee's knowledge")
    experience_level = models.ForeignKey(ExperienceLevel, on_delete=models.CASCADE,help_text= "employee's knowledge")
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE,help_text= "employee's knowledge", related_name= "employee_experience_level")
    
    class meta:
        db_table = 'sth_stage_experience_level'
    
    def __str__(self):
        return str(self.employee) +"-"+str(self.stage) +"-"+ str(self.experience_level)

class Team(Base):
    name = models.CharField(max_length=200, help_text= "team's name")
    description = models.TextField(help_text= "team's description")
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE,help_text= "organization")
    responsible = models.ForeignKey(Employee, on_delete=models.CASCADE,help_text= "Employee")