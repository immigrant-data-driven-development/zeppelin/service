from django.shortcuts import render

from .models import *
from .serializers import *
from rest_framework import viewsets

class CategoryViewSet(viewsets.ModelViewSet):
    
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

class ElementViewSet(viewsets.ModelViewSet):
    
    queryset = Element.objects.all()
    serializer_class = ElementSerializer
