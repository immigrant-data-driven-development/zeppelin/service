from django.contrib import admin
from .models import Category, Element


class DimensionAdmin(admin.ModelAdmin):
    list_display = ('name', 'description',)
    list_display_links = ('name',)
    ordering = ['name']
    search_fields = ['name']

class ElementAdmin(admin.ModelAdmin):
    list_display = ('name', 'description','dimension',)
    list_filter = ('dimension',)
    list_display_links = ('name',)
    ordering = ['name']
    search_fields = ['name','dimension',]


admin.site.register(Category,DimensionAdmin)
admin.site.register(Element,ElementAdmin)
