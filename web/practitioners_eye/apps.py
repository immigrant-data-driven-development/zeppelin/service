from django.apps import AppConfig


class PractitionersEyeConfig(AppConfig):
    name = 'practitioners_eye'
